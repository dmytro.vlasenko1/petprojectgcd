//
//  VideoPreviewTableViewCell.swift
//  PetProjectItomych
//
//  Created by Dmytro Vlasenko on 18.01.2022.
//

import UIKit

final class VideoPreviewTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var previewTitleLabel: UILabel!
    @IBOutlet private weak var metaDataLabel: UILabel!
    @IBOutlet private weak var previewImageView: UIImageView!

    func configure(previewImage: UIImage, dateCreationTitle: String, previewTitle: String) {
        selectionStyle = .none
        previewImageView.image = previewImage
        metaDataLabel.text = dateCreationTitle
        previewTitleLabel.text = previewTitle
    }
}
