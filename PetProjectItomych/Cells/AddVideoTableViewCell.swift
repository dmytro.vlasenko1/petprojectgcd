//
//  AddVideoTableViewCell.swift
//  PetProjectItomych
//
//  Created by Dmytro Vlasenko on 18.01.2022.
//

import UIKit

final class AddVideoTableViewCell:  UITableViewCell {


    @IBOutlet private weak var createVideoButton: UIButton!

    private var onCreateVideoButtonTap: (() -> Void)?

    func configure(buttonTitle: String, action: (() -> Void)?) {
        selectionStyle = .none
        createVideoButton.setTitle(buttonTitle, for: .normal)
        onCreateVideoButtonTap = action
    }

    @IBAction func onCreateVideoTap(_ sender: UIButton) {
        onCreateVideoButtonTap?()
    }
}
