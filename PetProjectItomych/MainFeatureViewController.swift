//
//  ViewController.swift
//  PetProjectItomych
//
//  Created by Dmytro Vlasenko on 09.01.2022.
//

import UIKit
import AVFoundation
import AVKit
import MobileCoreServices

final class MainFeatureViewController: UIViewController {

    private enum Constants {
        static let addVideoCell = "AddVideoTableViewCell"
        static let videoPreviewCell = "VideoPreviewTableViewCell"
    }

    // MARK: - Outlets

    @IBOutlet private weak var videoView: UIView!
    @IBOutlet private  weak var tableView: UITableView!

    // MARK: - Properties
    private var repository: MainFeatureRepositoryProtocol = MainFeatureRepository()
    private var model: [VideoResponseModel] =  []
    private let thumbnailManager = ThumbnailManager()
    private var avpController = AVPlayerViewController()
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "YY, MMM d, HH:mm:ss"
        return formatter
    }()
    private let submitVideoQueue = DispatchQueue(label: "submit-video-queue", attributes: .concurrent)


    override func viewDidLoad() {
        super.viewDidLoad()
        //getDataFromRealm()
        configureTableView()
        getDataSource()
    }

    private func getDataFromRealm() {
        repository.retrieveDataFromRealm { [weak self] model in
            self?.model = model
            self?.tableView.reloadData()
        } onError: { error in
            print(error)
        }
    }

    private func getDataSource() {
        repository.getDataFromFirebase {[weak self] model in
            self?.model = model
            self?.tableView.reloadData()
        } onError: { error in
            print(error)
        }
    }
    private func configureTableView() {
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.register(UINib(nibName: Constants.addVideoCell, bundle: nil), forCellReuseIdentifier: Constants.addVideoCell)
        tableView.register(UINib(nibName: Constants.videoPreviewCell, bundle: nil), forCellReuseIdentifier: Constants.videoPreviewCell)

    }

    private func recordVideo() {
        let mediaPickerViewController = UIImagePickerController()
        mediaPickerViewController.delegate = self
        mediaPickerViewController.sourceType = .camera
        mediaPickerViewController.mediaTypes = [kUTTypeMovie as String]
        mediaPickerViewController.videoQuality = .typeHigh
        self.present(mediaPickerViewController, animated: true)
    }


}

extension MainFeatureViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == model.count {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.addVideoCell, for: indexPath) as? AddVideoTableViewCell else {return UITableViewCell()}
            cell.configure(buttonTitle: "Submit new video", action: recordVideo)
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.videoPreviewCell, for: indexPath) as? VideoPreviewTableViewCell else {return UITableViewCell()}
            if let image = model[indexPath.row].videoThumbnail {
                cell.configure(previewImage: image, dateCreationTitle: dateFormatter.string(from: model[indexPath.row].currentDate), previewTitle: model[indexPath.row].videoName)
            } else {
                cell.configure(previewImage:  UIImage(systemName: "photo")!, dateCreationTitle: dateFormatter.string(from: model[indexPath.row].currentDate), previewTitle: model[indexPath.row].videoName)
                // TODO: Move logic and model to repository
                if !model[indexPath.row].isLoadingThumbnailImage {
                    model[indexPath.row].isLoadingThumbnailImage = true
                    thumbnailManager.getThumbnailImage(videoURL: model[indexPath.row].videoURL) { [weak self] image in
                        self?.model[indexPath.row].videoThumbnail = image
                        self?.model[indexPath.row].isLoadingThumbnailImage = false
                        self?.tableView.reloadData()
                    } onImageFetchFailure: {[weak self] error in
                        print(error)
                        self?.model[indexPath.row].isLoadingThumbnailImage = false
                        self?.tableView.reloadData()
                    }
                }
            }
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != model.count {
            let url = model[indexPath.row].videoURL
            let player = AVPlayer(url: url)
            self.videoView.subviews.forEach { element in
                element.removeFromSuperview()
            }
            avpController.player = player
            avpController.view.frame.size.height = videoView.frame.size.height
            avpController.view.frame.size.width = videoView.frame.size.width
            self.videoView.addSubview(avpController.view)
        }
    }

}



extension MainFeatureViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if info[UIImagePickerController.InfoKey.mediaType] as? String == kUTTypeMovie as String,
           let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
            picker.dismiss(animated: true) { [weak self] in
                let alert = UIAlertController(title: "", message: "What the title of the video should be?", preferredStyle: .alert)
                alert.addTextField()
                alert.addAction(UIAlertAction(title: "Submit", style: .default, handler: { action in
                    let videoTitle = alert.textFields?.first?.text ?? "No title"
                    self?.submitVideoQueue.async {
                        self?.repository.uploadVideoToFirebase(fileName: videoTitle, fileURL: videoURL) { model in
                            DispatchQueue.main.async {
                                self?.model.append(model)
                                self?.tableView.reloadData()
                            }
                        } onError: { error in
                            //self?.getDataFromRealm()
                        }
                    }
                }))
                self?.present(alert, animated: true, completion: nil)
            }
            picker.dismiss(animated: true)
        }
    }
}

