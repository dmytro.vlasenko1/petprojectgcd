//
//  MainFeatureRepository.swift
//  PetProjectItomych
//
//  Created by Dmytro Vlasenko on 20.01.2022.
//

import UIKit
import RealmSwift
import FirebaseStorage
import FirebaseFirestore
import AVFoundation

struct VideoResponseModel {
    let videoURL: URL
    let videoName: String
    var videoThumbnail: UIImage? = nil
    var isLoadingThumbnailImage = false
    let creationDateTimestamp: Double
    lazy var currentDate: Date = {
        Date(timeIntervalSince1970: creationDateTimestamp)
    }()
}

class VideoModelRealm: Object {
    @objc dynamic var videoURL: String = ""
    @objc dynamic var videoName: String = ""
    @objc dynamic var creatingDate: Double = 0

    func convertToStruct() -> VideoResponseModel {
        let documentsString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/" + self.videoURL
        return VideoResponseModel(videoURL: URL(string: documentsString)!, videoName: self.videoName, creationDateTimestamp: self.creatingDate)
    }
}


enum MainFeatureRepositoryError: Error {
    case missingDownloadURL
    case emptyDatabase
}

protocol MainFeatureRepositoryProtocol {
    func uploadVideoToFirebase(fileName: String, fileURL: URL, onSuccess: ((VideoResponseModel) -> Void)?, onError: ((Error) -> Void)?)
    func getDataFromFirebase(onSuccess: (([VideoResponseModel]) -> Void)?, onError: ((Error) -> Void)?)
    func saveVideoToRealm(data: VideoModelRealm, onSuccess: (() -> Void)?, onError: ((Error) -> Void)?)
    func retrieveDataFromRealm(onSuccess: (([VideoResponseModel]) -> Void)?, onError: ((Error) -> Void)?)
}

final class MainFeatureRepository: MainFeatureRepositoryProtocol {

    private let mainOperationQueue = OperationQueue()
    private let realm = try! Realm()

    func uploadVideoToFirebase(fileName: String, fileURL: URL, onSuccess: ((VideoResponseModel) -> Void)?, onError: ((Error) -> Void)?) {
        let name = "\(UUID().uuidString).mp4"
        let dispatchgroup = DispatchGroup()
        dispatchgroup.enter()
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let outputurl = documentsURL.appendingPathComponent(name)
        var videoURL = outputurl
        self.convertVideo(toMPEG4FormatForVideo: fileURL, outputURL: outputurl) { (session) in
            guard let url = session.outputURL else {
                dispatchgroup.leave()
                return

            }
            videoURL = url
            dispatchgroup.leave()
        }
        dispatchgroup.wait()
// IDK HOW TO SAVE VIDEO TO DOCUMENTS DIRECTORY SEEMS LIKE A 200IQ TASK TO DO 
//        let path = try! FileManager.default.url(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask, appropriateFor: nil, create: false)
//        let videoPath = path.appendingPathComponent("/videoFileName.mp4")
        let data = NSData(contentsOf: videoURL as URL)
//        do {
//            try data?.write(to: videoPath)
//        } catch {
//            onError?(error)
//        }
//        print(FileManager().fileExists(atPath: videoPath.absoluteString))
//        data?.write(toFile: videoPath.absoluteString, atomically: false)
//        print(FileManager().fileExists(atPath: videoPath.absoluteString))
        let storageRef = Storage.storage().reference().child(name)
        if let uploadData = data as Data? {
            storageRef.putData(uploadData, metadata: nil, completion: { [weak self] (metadata, error) in
                if let error = error {
                    onError?(error)
                } else {
                    let currentDate = Date().timeIntervalSince1970
                    let saveToRealmOperationBlock = BlockOperation { [weak self] in
                        let model = VideoModelRealm()
                        model.videoURL = name
                        model.videoName = fileName
                        model.creatingDate = currentDate
                        self?.saveVideoToRealm(data: model, onSuccess: nil, onError: nil)
                    }
                    saveToRealmOperationBlock.qualityOfService = .background
                    saveToRealmOperationBlock.queuePriority = .low
                    self?.mainOperationQueue.addOperation(saveToRealmOperationBlock)
                    storageRef.downloadURL { url, error in
                        if let error = error {
                            onError?(error)
                        } else {
                            guard let url = url else {
                                onError?(MainFeatureRepositoryError.missingDownloadURL)
                                return
                            }
                            let dataSample: [String : Any] = ["videoURL" : url.absoluteString,
                                                              "videoName" : fileName,
                                                              "timeCreated" : currentDate]
                            Firestore.firestore().collection("Videos").addDocument(data: dataSample) { error in
                                if let error = error {
                                    onError?(error)
                                } else {
                                    
                                    onSuccess?(VideoResponseModel(videoURL: URL(string: url.absoluteString)!, videoName: fileName, creationDateTimestamp: currentDate))
                                }
                            }
                        }
                    }
                }
            })
        }
    }

    func getDataFromFirebase(onSuccess: (([VideoResponseModel]) -> Void)?, onError: ((Error) -> Void)?) {
        Firestore.firestore().collection("Videos").getDocuments { snapshot, error in
            if let error = error {
                onError?(error)
            } else {
                var model: [VideoResponseModel] = []
                snapshot?.documents.forEach({ documentSnapshot in
                    let dict = documentSnapshot.data()
                    // force-unwrap for demo purposes only
                    model.append(VideoResponseModel(videoURL:URL(string:dict["videoURL"] as! String)!, videoName: dict["videoName"] as! String, creationDateTimestamp: dict["timeCreated"] as! Double))
                })
                onSuccess?(model)
            }
        }
    }

    func saveVideoToRealm(data: VideoModelRealm, onSuccess: (() -> Void)?, onError: ((Error) -> Void)?) {
        DispatchQueue.main.async { [weak self] in
            self?.realm.beginWrite()
            self?.realm.add(data)
            // TODO: ERROR HANDLING
            try! self?.realm.commitWrite()
            onSuccess?()
        }

    }

    func retrieveDataFromRealm(onSuccess: (([VideoResponseModel]) -> Void)?, onError: ((Error) -> Void)?) {
        let fetchedItems = realm.objects(VideoModelRealm.self)
        let converted: [VideoResponseModel] = fetchedItems.map({ $0.convertToStruct() })
        converted.isEmpty ? onError?(MainFeatureRepositoryError.emptyDatabase) : onSuccess?(converted)
    }



    private func convertVideo(toMPEG4FormatForVideo inputURL: URL, outputURL: URL, handler: @escaping (AVAssetExportSession) -> Void) {
        let asset = AVURLAsset(url: inputURL as URL, options: nil)
        let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mp4
        exportSession.exportAsynchronously(completionHandler: {
            handler(exportSession)
        })
    }
}
