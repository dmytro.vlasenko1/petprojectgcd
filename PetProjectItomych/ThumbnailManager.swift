//
//  ThumbnailManager.swift
//  PetProjectItomych
//
//  Created by Dmytro Vlasenko on 23.01.2022.
//

import AVFoundation
import UIKit

class ThumbnailManager {

    private let thumbnailQueue = DispatchQueue(label: "thumbnail-manager-queue", attributes: .concurrent)

    func getThumbnailImage(videoURL: URL, onImageFetch: @escaping (UIImage) -> Void, onImageFetchFailure: ((Error) -> Void)? = nil) {
        let asset: AVAsset = AVAsset(url: videoURL)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        let timestamp = CMTimeMake(value: 1, timescale: 60)
        thumbnailQueue.async {
            do {
                let thumbnailImage = try imageGenerator.copyCGImage(at: timestamp, actualTime: nil)
                DispatchQueue.main.async {
                    let image = UIImage(cgImage: thumbnailImage)
                    onImageFetch(image)
                }
            } catch {
                DispatchQueue.main.async {
                    onImageFetchFailure?(error)
                }
            }
        }
    }
}
